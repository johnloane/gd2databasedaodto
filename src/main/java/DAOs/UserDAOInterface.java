package DAOs;

import DTOs.User;
import Exceptions.DAOException;

import java.util.List;

/* Declares the methods that all UserDAO types must implement. It doesn't matter whether we will use Mysql or Postgres or Mongo
   Classes from the Business Layer should use references to this interface to avoid dependencies on the underlying concrete classes
 */
public interface UserDAOInterface {
    public List<User> findAllUsers() throws Exceptions.DAOException;
    public User findUserByUsernamePassword(String uname, String pword) throws DAOException;
}
