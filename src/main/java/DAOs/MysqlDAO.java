package DAOs;

import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/* Implements functionality that is common to all Mysql
DAOs i.e connect and disconnect
All Mysql DAOs will extend this class. This way we can avoid duplicating this code in every DAO
 */
public class MysqlDAO {
    public Connection getConnection() throws DAOException
    {
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:8889/gd2OOPCA4";
        String username = "root";
        String password = "root";
        Connection con = null;

        try{
            con = DriverManager.getConnection(url, username, password);
        }
        catch(SQLException ex){
            System.out.println("Connection failed " + ex.getMessage());
            System.exit(1);
        }
        return con;
    }

    public void closeConnection(Connection con) throws DAOException{
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        }
        catch(SQLException e){
            System.out.println("Failed to free the connection: " + e.getMessage());
            System.exit(1);
        }
    }
}
