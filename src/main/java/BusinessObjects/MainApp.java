package BusinessObjects;

import DAOs.MySqlUserDAO;
import DAOs.UserDAOInterface;
import DTOs.User;
import Exceptions.DAOException;

import java.util.List;

/* This app demonstrates how to use a DAO to separate out business logic and database specific logic.
   It uses DAOs, DTOs and a DAOInterface to define a contract between the business layer and DAOs.
   Use a DAO to abstract and encapsulate all access to the data source. The DAO manages the connection to the data source.
   Here we use one DAO per table
 */
public class MainApp {
    public static void main(String[] args) {
         UserDAOInterface IUserDAO = new MySqlUserDAO();
         /* I for interface here. Notice that the userDAO reference is an interface. This allows for different concrete implements.
            If the interface doesn't need to change then none of the code below needs to change
          */
         try{
             List<User> users = IUserDAO.findAllUsers();
             if(users.isEmpty()){
                 System.out.println("There are no users");
             }
             for(User user : users){
                 System.out.println("User: " + user.toString());
             }

             //test findUserByUsernamePassword
             //test with good username and password
             User user = IUserDAO.findUserByUsernamePassword("somejohns", "nojohns");
             if(user != null){
                 System.out.println("User found: " + user);
             }else{
                 System.out.println("User with that username and password was not found");
             }
         }catch(DAOException e){
             e.printStackTrace();
         }
    }
}
