package Exceptions;

import java.sql.SQLException;
/* A homemade exception to report exceptions in the DAO*/

public class DAOException extends SQLException {
    public DAOException() {
    }

    public DAOException(String eMessage){
        super(eMessage);
    }
}
